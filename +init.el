;;; ~/.doom.d/+init.el -*- lexical-binding: t; -*-

(setq comp-async-jobs-number (string-to-number
                              (shell-command-to-string "nproc")))
