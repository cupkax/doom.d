(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("fce3524887a0994f8b9b047aef9cc4cc017c5a93a5fb1f84d300391fba313743" default)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(doom-modeline-buffer-modified ((t (:foreground "grey"))))
 '(org-document-title ((t (:height 1.2))))
 '(outline-1 ((t (:weight extra-bold :height 1.1))))
 '(outline-2 ((t (:weight bold :height 1.09))))
 '(outline-3 ((t (:weight bold :height 1.07))))
 '(outline-4 ((t (:weight semi-bold :height 1.05))))
 '(outline-5 ((t (:weight semi-bold :height 1.03))))
 '(outline-6 ((t (:weight semi-bold :height 1.01))))
 '(outline-7 ((t (:weight semi-bold :height 1.01))))
 '(outline-8 ((t (:weight semi-bold))))
 '(outline-9 ((t (:weight semi-bold)))))
